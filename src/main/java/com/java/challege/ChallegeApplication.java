package com.java.challege;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallegeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallegeApplication.class, args);
	}

}
